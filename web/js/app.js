let ComponentListProducts = {
  props   : ['product'],
  template: ` <div class="item" :class="{isChecked : product.checked}">
                    <div class="checked">
                        <label :for="'checkbox-' + product.id" />
                        <input :id="'checkbox-' + product.id" type="checkbox" @click="updateCheckProduct(product, $event)" :checked="product.checked"/>
                    </div>
                    <div class="name">{{ product.name }}</div>
                    <div class="price">
                        <div v-if=product.checked class="ui right icon input">
                            <input class="price" v-model="product.price" @change="updatePriceProduct(product, $event)" />
                            <i class="euro sign icon"></i>
                        </div>
                    </div>
                    <div class="delete"><button class="ui negative basic button" @click="deleteProduct(product.id)"><i class="trash icon"></i></button></div>
                </div>`,
  methods : {
    deleteProduct (id) {
      this.$parent.deleteProduct(id)
    },
    updateCheckProduct (product, $event) {
      this.$parent.updateCheckProduct(product, $event)
      
    },
    updatePriceProduct (product, $event) {
      this.$parent.updatePriceProduct(product, $event)
    }
  }
}

let ComponentListAutoCompletion = {
  props   : ['product'],
  template: `
        <div class="itemAuto" @click="addProduct(product)">{{product.name}}</div>
    `,
  
  methods: {
    addProduct (product) {
      this.$parent.addProduct(product)
    }
  }
}

let app = new Vue({
  el        : '#app',
  components: {
    'listproducts'       : ComponentListProducts,
    'list-autocompletion': ComponentListAutoCompletion
  },
  
  data: {
    nameProduct       : '',
    listProducts      : [],
    listAutoCompletion: null,
    budget            : null
  },
  
  methods: {
    addProduct (product = null) {
      if (!product) {
        product = {
          id     : this.getNewId(),
          name   : this.nameProduct,
          price  : 0,
          checked: false
        }
        this.listProducts.push(product)
      } else {
        Object.assign(clone = {}, product)
        clone.id = this.getNewId()
        this.listProducts.push(clone)
      }
      this.nameProduct = ''
      this.updateListAuto()
    },
    
    getNewId () {
      if (this.listProducts.length === 0) {
        return 0
      }
      
      let lastProduct = this.listProducts[this.listProducts.length - 1]
      return lastProduct.id + 1
    },
    
    deleteProduct (id) {
      this.listProducts.forEach((p, index) => {
        if (p.id === id) {
          this.listProducts.splice(index, 1)
        }
      })
      this.updateListAuto()
    },
    
    updateCheckProduct (product, event) {
      product.checked = event.target.checked
    },
    
    updatePriceProduct (product, event) {
      if (!isNaN(parseInt(event.target.value))) {
        product.price = event.target.value
      }
    },
    
    updateListAuto () {
      this.listAutoCompletion = []
      if (this.nameProduct && this.listProducts.length > 0) {
        this.listProducts.forEach((e) => {
          let regex = new RegExp('^' + e.name.split('').join('?') + '?$')
          if (regex.test(this.nameProduct)) {
            this.listAutoCompletion.push(e)
          }
        })
      }
    }
  },
  
  computed: {
    budgetReached () {
      return this.budget && this.budget < this.grandTotal
    },
    
    grandTotal () {
      let grandTotal = 0
      this.listProducts.forEach((p) =>
        grandTotal += p.checked ? parseFloat(p.price) : 0
      )
      return grandTotal
    },
    
    listAutoCompletionComputed () {
      return this.nameProduct && this.listAutoCompletion && this.listAutoCompletion.length > 0
    }
  },
  
  watch: {
    listProducts: {
      handler () {
        window.localStorage.setItem('listProducts', JSON.stringify(this.listProducts))
      },
      deep: true
    }
  },
  
  mounted () {
    if (window.localStorage.getItem('listProducts')) {
      this.listProducts = JSON.parse(window.localStorage.getItem('listProducts'))
    }
  }
})